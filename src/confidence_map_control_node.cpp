#include <ros/ros.h>
#include<image_transport/image_transport.h> // include everything needed to publish and subscribe to images topics
#include <sensor_msgs/image_encodings.h>
#include<sensor_msgs/Image.h>
//allows to display images using opencv simple gui
#include <opencv2/highgui/highgui.hpp> 
#include <cv_bridge/cv_bridge.h>
#include <opencv2/core/mat.hpp>
#include <vector>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <opencv2/imgproc/imgproc.hpp>
#include <chrono>
using namespace std::chrono;

static const std::string OPENCV_WINDOW = "Image window";
class ConfidenceMap
{
    ros::NodeHandle nh_; 
    image_transport::ImageTransport it_;
    image_transport::Subscriber image_sub_ ; 
    image_transport::Publisher image_pub_;
    cv_bridge::CvImagePtr cv_ptr;
    cv::Mat img;
    int Ln;//width
    int An;//Height
    int iter = 1;
    float t_sum = 0;
    float t_average = 0;
    public: 
        ConfidenceMap(): it_(nh_)
        {
            image_pub_ = it_.advertise("/confidenceMap",1);

            image_sub_ = it_.subscribe("/frames",1,&ConfidenceMap::imageCallback,this);
            cv::namedWindow(OPENCV_WINDOW);
        }
    ~ConfidenceMap()
    {
        cv::destroyWindow(OPENCV_WINDOW);
    }

    void imageCallback(const sensor_msgs::ImageConstPtr& msg)
    {
        
        try 
        {
            cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::MONO8);
        }
        catch(cv_bridge::Exception& e)
        {
            ROS_ERROR("cv_bridge exception %s", e.what());
        }

        
        crop();
        confidence();        
    }
    void crop()
    {
        const int threshVal = 20;
        const float borderThresh = 0.05f; // 5%
        cv::Mat thresholded;
        cv::threshold(cv_ptr->image, thresholded, threshVal, 255, cv::THRESH_BINARY);
        cv::morphologyEx(thresholded, thresholded, cv::MORPH_CLOSE,
        cv::getStructuringElement(cv::MORPH_RECT, cv::Size(3, 3)),
        cv::Point(-1, -1), 2, cv::BORDER_CONSTANT, cv::Scalar(0));
        cv::Point tl, br;

        for (int row = 0; row < thresholded.rows; row++)
        {
            if (cv::countNonZero(thresholded.row(row)) > borderThresh * thresholded.cols)
            {
                tl.y = row;
                break;
            }
        }

        for (int col = 0; col < thresholded.cols; col++)
        {
          if (cv::countNonZero(thresholded.col(col)) > borderThresh * thresholded.rows)
          {
            tl.x = col;
            break;
          }
        }

        for (int row = thresholded.rows - 1; row >= 0; row--)
        {
          if (cv::countNonZero(thresholded.row(row)) > borderThresh * thresholded.cols)
          {
            br.y = row;
            break;
          }
        }

        for (int col = thresholded.cols - 1; col >= 0; col--)
        {
          if (cv::countNonZero(thresholded.col(col)) > borderThresh * thresholded.rows)
          {
            br.x = col;
            break;
          }
        }

        cv::Rect roi(tl, br);
        img = cv_ptr->image(roi);
          }

    void confidence()
    {
        auto start = high_resolution_clock::now();
        An = img.size[0];
        Ln  = img.size[1];
        cv::Mat conf(An, Ln, CV_8UC1);
        double temp =0;
        double min = 255;
        double max = 0;
        for (int i = 0; i < An; ++i )
        {
            for (int j = 0; j < Ln ; ++j)
            {
                
                if ((double(img.at<uchar>(i,j))) < min) min = double(img.at<uchar>(i,j));
                if ((double(img.at<uchar>(i,j))) > max) max = double(img.at<uchar>(i,j));
            }
            
        }
        //std::cout<<"[AN] : " << An <<std::endl;
        //std::cout<<"[Ln] : " << Ln <<std::endl;
        if (max == 0)  std::cout<<"[ERROR] : Denomiator is "<<std::endl;
        for(int j = 0; j<Ln; ++j)
        {
            float sum = 0;
            for (auto i = 0; i < An; ++i)
            {   
                sum =sum + pow(((double(img.at<uchar>(i,j))-min)/(max-min)),4);
                //std::cout<<sum<<std::endl;
            }
            
            float val = sum;
            conf.at<uchar>(0,j) = uchar(255);
            for (int i = 1; i < An; ++i)
            {   
                
                val = val - pow(((double(img.at<uchar>(i-1,j))-min)/(max-min)),4);
                if ( sum  != 0){
                    temp = (val/sum)*255;
                    //temp = (val/sum);                
                    conf.at<uchar>(i,j) = temp;
                } 
            }
        }
        auto stop = high_resolution_clock::now();
        auto duration = duration_cast<milliseconds>(stop - start);
        
        t_sum += float(duration.count());
        t_average = float (t_sum/iter);
        std::cout<<"[AVG EXE Time] : "<< t_average<<std::endl;
        std::cout<<"[Number of Frames] : "<< iter<< std::endl;
        sensor_msgs::ImagePtr msg = cv_bridge::CvImage(std_msgs::Header(), "mono8", conf).toImageMsg();
        
        iter ++;
        image_pub_.publish(msg);
    }
  
};       


int main(int argc, char** argv)
{
    ros::init(argc , argv, "image_listener");
    ConfidenceMap cm;
    ros::spin();
    return 0;
}