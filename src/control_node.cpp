#include <ros/ros.h>
#include<image_transport/image_transport.h> // include everything needed to publish and subscribe to images topics
#include <sensor_msgs/image_encodings.h>
#include<sensor_msgs/Image.h>
//allows to display images using opencv simple gui
#include <opencv2/highgui/highgui.hpp> 
#include <cv_bridge/cv_bridge.h>
#include <opencv2/core/mat.hpp>
#include <vector>
#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <opencv2/imgproc/imgproc.hpp>
#include <chrono>
using namespace std ;
static const std::string OPENCV_WINDOW = "Image window";
class Control{
    ros::NodeHandle nh_; 
    image_transport::ImageTransport it_;
    image_transport::Subscriber conf_subscriber_ ; 
    image_transport::Publisher conf_pub_;
    cv_bridge::CvImagePtr cv_ptr;
    cv::Mat confidence;
    cv::Mat conf;
    float alpha = 0.121;
    float beta = 0.0078;
    vector<float>c_omega; //to compute the weighted barycenter in pixel 
    float theta_c;//The angle at which we have the maximum confidence
    float theta_t;//The angle of the target
    float height, width; // height and width of the image to cimpute the weighterd barycenter
    float u_c, v_c ; //pixel coordinates of the barycenter
    float x_c, y_c; // cartesian coordinates of the barycenter
    
    public :
        Control() : it_(nh_)
        {
            conf_pub_= it_.advertise("/Control_image",1);
            conf_subscriber_ = it_.subscribe("/confidenceMap", 1, &Control::confCallback, this);
            cv::namedWindow(OPENCV_WINDOW);
        }
        ~Control() 
        {
            cv::destroyWindow(OPENCV_WINDOW);
        } 
        void confCallback(const sensor_msgs::ImageConstPtr& msg)
        {
        
            try 
            {
                cv_ptr = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::MONO8);
            }
            catch(cv_bridge::Exception& e)
            {
                ROS_ERROR("cv_bridge exception %s", e.what());
            } 
            error_computation();    
        }
        float Moments (int d_0, int d_1 )
        {
            float moment = 0;
            for (int j = 0; j < width ; j ++)
            {
                for( int i = 0; i < height ; i++)
                {
                    moment += pow(i,d_0)* pow(j,d_1) * float(cv_ptr->image.at<uchar> (i,j))/255;
                }
            }
            return moment;

        }

        float weighted_BC()
        {
            height = cv_ptr -> image.size[0];
            width = cv_ptr -> image.size[1];
            float M00 = Moments(0,0);
            float M10 = Moments(1,0);
            float M01 = Moments(0,1);
            
            
            cv::Point P2;
            P2.x =  (int)round(M01/M00);
            P2.y =  (int)round(M10/M00);
            float temp = pow((alpha*(162- P2.x)),2) + pow((beta*(P2.y)),2);
            float r = sqrt(temp);
            float theta_C = atan2(asin((alpha*(P2.x-162))/r),acos((beta*(P2.y))/r) );
            
            return theta_C;
            

        }
        void error_computation()
        {
            theta_c =  weighted_BC ();
            int length = 100000;
            cv::Point P1(width/2,5);
            cv::Point P;
            P.x = (int)round(P1.x + length * sin(theta_c));
            P.y = (int)round(P1.y + length * cos(theta_c));
            cout<<theta_c<<endl;
            cv::line(cv_ptr->image, P1, P, (255,255,255), 2);
            sensor_msgs::ImagePtr msg_out = cv_bridge::CvImage(std_msgs::Header(), "mono8", cv_ptr->image).toImageMsg();
            conf_pub_.publish(msg_out);
        }

};




int main(int argc, char** argv)
{
    ros::init(argc , argv, "control_node");
    Control C;
    ros::spin();
    return 0;
}