
import numpy as np
import cv2
from math import *
import time

import rospy 
from sensor_msgs.msg import Image
from cv_bridge import CvBridge
import sys




class Confidence_map : 
    def __init__(self) :
        self.scanLineNumber = 128
        self.fielOfView = 0.039
        self.xResolution = 0
        self.yResolution = 0
        self.BmodeSampleNumber = 104
        self.BmodeImage = None
        self.imap = None
        self.jmap = None
        self.height = 0
        self.width = 0 
        self.alpha = 0.121
        self.beta = 0.000385
        self.img = None
        self.imgmsg = None
        #debug variables
        self.debug = False
        self.t0 = 0
        self.t_exec = 0
        self.t_avg = 0
        self.count = 0
        self.t_sum = 0
        
    def clear(self):
        self.img = None
    def mapping (self,Bmode) :
        self.imap = np.zeros((self.BmodeSampleNumber,self.scanLineNumber))
        self.jmap = np.zeros((self.BmodeSampleNumber,self.scanLineNumber))
        for u in range (self.BmodeSampleNumber):
            for v in range(self.scanLineNumber):
                self.imap[u][v] = self.alpha * (u - self.xResolution)
                self.jmap[u][v] = self.beta * v



    def interpolateLinear(self, Bmode, x, y):
        x1 = floor(x)
        x2 = ceil(x)
        y1 = floor(y)
        y2 = ceil(y)

        if (((0 <= x) and x < self.height) and ((0 <= y) and (y<=self.width))):
            #check if the indices are in the image field
            if (x1 < 0):
                x1 = x1 + 1
            if (y1 < 0):
                y1 = y1 + 1
            if (x2>= self.height):
                x2 = x2 - 1
            if (y2 >= self.width):
                y2 = y2 - 1

            if (x1 == x2):
                val1 = Bmode[x1,y1]
                val2 = Bmode[x2,y2]
            else:
                val1 = (x2 - x) * Bmode[x1, y1] + (x - x1) * Bmode[x2, y1]
                val2 = (x2 - x) * Bmode[x1, y2] + (x - x1) * Bmode[x2, y2]

            if (y1 == y2) : 
                return val1
            else :
                return (y2-y) * val1 + (y - y1) * val2
            return 0

    def convertTo2RF(self, Bmode) :
        image_converted = np.zeros((self.BmodeSampleNumber, self.scanLineNumber))
        self.mapping(Bmode)
        for u in range (self.BmodeSampleNumber) :
            for v in range (self.scanLineNumber):
                i = self.imap[u][v]
                j = self.jmap[u][v]
                image_converted[u][v] = self.interpolateLinear(Bmode, i, j)
        return image_converted
    
    def confidence(self):
        #an = self.BmodeSampleNumber
        #ln = self.scanLineNumber
        min = 255
        max = 0
        an = self.height
        ln = self.width
        c = np.zeros((an,ln))
        '''
            find min and max 
        ''' 
        for i in range(an):
            for j in range (ln):
                if (self.img[i][j] < min) : min = self.img[i][j]
                if (self.img[i][j] > max) : max = self.img[i][j]
        if (cm.debug) :
            print('[An]  : ', an)
            print('[Ln]  : ', ln)
            print('[MIN] : ', min)
            print('[MAX] : ', max)
        #scan line integration
        for j in range(ln):
            sum = 0
            for i in range(an) :
               sum = sum + ((self.img[i][j]-min)/(max-min))**2
               
            val = sum
            for i in range(1,an):
                val = val - ((self.img[i-1][j]- min)/ (max - min))**2
                c[i][j] = val / sum
                c[i][j] = c[i][j] * 255
                if (c[i][j] < 10**(-10)) : c[i][j] = 0 
        

        return c 

cm = Confidence_map()

def crop(img):
    
    thresh = cv2.threshold(img, 0, 255, cv2.THRESH_BINARY)[1]

    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (15,15))
    morph = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, kernel)
    
    # get bounding box coordinates from largest external contour
    contours = cv2.findContours(morph, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    contours = contours[0] if len(contours) == 2 else contours[1]
    big_contour = max(contours, key=cv2.contourArea)
    x,y,w,h = cv2.boundingRect(big_contour)

    # crop image contour image
    result = img.copy()
    result = result[y:y+h, x:x+w]
    return result 

pub = rospy.Publisher("/confidence_map", Image, queue_size= 1)
def conf_map_node():
    #bridge = CvBridge()
    
    rospy.init_node("confidence_map")
    rospy.loginfo ('[STARTED] : Confidence map node')
    rospy.Subscriber('/frames', Image, img_in)
    rospy.spin()
    #rate = rospy.Rate(100) #2Hz
    '''while not rospy.is_shutdown():

        rospy.Subscriber('/frames', Image, img_in)
        if (cm.debug) : cm.t0 = time.time()

        if (cm.imgmsg != None) :
            cm.img = bridge.imgmsg_to_cv2(cm.imgmsg, "mono8")
            cm.img = crop(cm.img)
            cm.height,cm.width = cm.img.shape

            conf = cm.confidence().astype(np.uint8)

            if (cm.debug):
                cm.count = cm.count + 1
                cm.t_exec = time.time() - cm.t0
                cm.t_sum = cm.t_sum + cm.t_exec    
                cm.t_avg = cm.t_sum / cm.count
                print("[INFO] Average execution time per frame : ", cm.t_avg * 1000 , "ms")
                rospy.loginfo("[INFO] : processing done")

            pub.publish(bridge.cv2_to_imgmsg(conf,"mono8"))
            rate.sleep()'''

def img_in(img_in):
    try:
        
        bridge = CvBridge()
        cm.imgmsg = img_in
        cm.img = bridge.imgmsg_to_cv2(cm.imgmsg, "mono8")
        cm.img = crop(cm.img)
        cm.height,cm.width = cm.img.shape
        if (cm.debug) : cm.t0 = time.time()
        conf = cm.confidence().astype(np.uint8)
        if (cm.debug):
            cm.count = cm.count + 1
            cm.t_exec = time.time() - cm.t0
            cm.t_sum = cm.t_sum + cm.t_exec    
            cm.t_avg = cm.t_sum / cm.count
            print("[INFO] Average execution time per frame : ", cm.t_avg * 1000, "ms")
            print("[Number of Freames]", cm.count)
            rospy.loginfo("[INFO] : processing done")

        pub.publish(bridge.cv2_to_imgmsg(conf,"mono8"))
    except Exception as err :
        print(err)





if __name__ == "__main__" :
    # import the cv2 library
    
# The function cv2.imread() is used to read an image.
    try:
        f = 0
        input = sys.argv
        if (len(input) == 3):
            if (str(input[1])== '-d') and (str(input[2]) == "True") : 
                cm.debug = True
            elif (str(input[1])== '-d') and (str(input[2]) == "False"):
                cm.debug = False
            else : print('[ERROR] : Unknown argument')
        
        elif (len(input)== 1) : cm.debug = False
        else : 
            print('[ERROR]: Wrong argument')
            print('List of arguments allowed: ')
            print('\t -d : debug mode') 
            f=1
        if(f!=1):
            conf_map_node()
    except Exception as err :
        print(err)

    